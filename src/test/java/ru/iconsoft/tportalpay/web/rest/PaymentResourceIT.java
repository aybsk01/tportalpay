package ru.iconsoft.tportalpay.web.rest;

import ru.iconsoft.tportalpay.TportalpayApp;
import ru.iconsoft.tportalpay.domain.Payment;
import ru.iconsoft.tportalpay.domain.Invoice;
import ru.iconsoft.tportalpay.repository.PaymentRepository;
import ru.iconsoft.tportalpay.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static ru.iconsoft.tportalpay.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import ru.iconsoft.tportalpay.domain.enumeration.PaymentStatus;
/**
 * Integration tests for the {@link PaymentResource} REST controller.
 */
@SpringBootTest(classes = TportalpayApp.class)
public class PaymentResourceIT {

    private static final String DEFAULT_PGA_TRX = "AAAAAAAAAA";
    private static final String UPDATED_PGA_TRX = "BBBBBBBBBB";

    private static final String DEFAULT_LANG_CODE = "AA";
    private static final String UPDATED_LANG_CODE = "BB";

    private static final Instant DEFAULT_CREATE_DT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATE_DT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final PaymentStatus DEFAULT_STATUS = PaymentStatus.COMPLETED;
    private static final PaymentStatus UPDATED_STATUS = PaymentStatus.PENDING;

    private static final String DEFAULT_EXT_RESULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_EXT_RESULT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_RRN = "AAAAAAAAAA";
    private static final String UPDATED_RRN = "BBBBBBBBBB";

    private static final Instant DEFAULT_TRANSACTION_DT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TRANSACTION_DT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_CARD_HOLDER = "AAAAAAAAAA";
    private static final String UPDATED_CARD_HOLDER = "BBBBBBBBBB";

    private static final String DEFAULT_AUTH_CODE = "AAAAAAAAAA";
    private static final String UPDATED_AUTH_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_PAN = "AAAAAAAAAA";
    private static final String UPDATED_PAN = "BBBBBBBBBB";

    private static final Boolean DEFAULT_FULL_AUTH = false;
    private static final Boolean UPDATED_FULL_AUTH = true;

    private static final BigDecimal DEFAULT_PAYMENT_AMOUNT = new BigDecimal(1);
    private static final BigDecimal UPDATED_PAYMENT_AMOUNT = new BigDecimal(2);

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restPaymentMockMvc;

    private Payment payment;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PaymentResource paymentResource = new PaymentResource(paymentRepository);
        this.restPaymentMockMvc = MockMvcBuilders.standaloneSetup(paymentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Payment createEntity(EntityManager em) {
        Payment payment = new Payment()
            .pgaTrx(DEFAULT_PGA_TRX)
            .langCode(DEFAULT_LANG_CODE)
            .createDt(DEFAULT_CREATE_DT)
            .dt(DEFAULT_DT)
            .status(DEFAULT_STATUS)
            .extResultCode(DEFAULT_EXT_RESULT_CODE)
            .rrn(DEFAULT_RRN)
            .transactionDt(DEFAULT_TRANSACTION_DT)
            .cardHolder(DEFAULT_CARD_HOLDER)
            .authCode(DEFAULT_AUTH_CODE)
            .pan(DEFAULT_PAN)
            .fullAuth(DEFAULT_FULL_AUTH)
            .paymentAmount(DEFAULT_PAYMENT_AMOUNT);
        // Add required entity
        Invoice invoice;
        if (TestUtil.findAll(em, Invoice.class).isEmpty()) {
            invoice = InvoiceResourceIT.createEntity(em);
            em.persist(invoice);
            em.flush();
        } else {
            invoice = TestUtil.findAll(em, Invoice.class).get(0);
        }
        payment.setInvoice(invoice);
        return payment;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Payment createUpdatedEntity(EntityManager em) {
        Payment payment = new Payment()
            .pgaTrx(UPDATED_PGA_TRX)
            .langCode(UPDATED_LANG_CODE)
            .createDt(UPDATED_CREATE_DT)
            .dt(UPDATED_DT)
            .status(UPDATED_STATUS)
            .extResultCode(UPDATED_EXT_RESULT_CODE)
            .rrn(UPDATED_RRN)
            .transactionDt(UPDATED_TRANSACTION_DT)
            .cardHolder(UPDATED_CARD_HOLDER)
            .authCode(UPDATED_AUTH_CODE)
            .pan(UPDATED_PAN)
            .fullAuth(UPDATED_FULL_AUTH)
            .paymentAmount(UPDATED_PAYMENT_AMOUNT);
        // Add required entity
        Invoice invoice;
        if (TestUtil.findAll(em, Invoice.class).isEmpty()) {
            invoice = InvoiceResourceIT.createUpdatedEntity(em);
            em.persist(invoice);
            em.flush();
        } else {
            invoice = TestUtil.findAll(em, Invoice.class).get(0);
        }
        payment.setInvoice(invoice);
        return payment;
    }

    @BeforeEach
    public void initTest() {
        payment = createEntity(em);
    }

    @Test
    @Transactional
    public void createPayment() throws Exception {
        int databaseSizeBeforeCreate = paymentRepository.findAll().size();

        // Create the Payment
        restPaymentMockMvc.perform(post("/api/payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payment)))
            .andExpect(status().isCreated());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeCreate + 1);
        Payment testPayment = paymentList.get(paymentList.size() - 1);
        assertThat(testPayment.getPgaTrx()).isEqualTo(DEFAULT_PGA_TRX);
        assertThat(testPayment.getLangCode()).isEqualTo(DEFAULT_LANG_CODE);
        assertThat(testPayment.getCreateDt()).isEqualTo(DEFAULT_CREATE_DT);
        assertThat(testPayment.getDt()).isEqualTo(DEFAULT_DT);
        assertThat(testPayment.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testPayment.getExtResultCode()).isEqualTo(DEFAULT_EXT_RESULT_CODE);
        assertThat(testPayment.getRrn()).isEqualTo(DEFAULT_RRN);
        assertThat(testPayment.getTransactionDt()).isEqualTo(DEFAULT_TRANSACTION_DT);
        assertThat(testPayment.getCardHolder()).isEqualTo(DEFAULT_CARD_HOLDER);
        assertThat(testPayment.getAuthCode()).isEqualTo(DEFAULT_AUTH_CODE);
        assertThat(testPayment.getPan()).isEqualTo(DEFAULT_PAN);
        assertThat(testPayment.isFullAuth()).isEqualTo(DEFAULT_FULL_AUTH);
        assertThat(testPayment.getPaymentAmount()).isEqualTo(DEFAULT_PAYMENT_AMOUNT);
    }

    @Test
    @Transactional
    public void createPaymentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = paymentRepository.findAll().size();

        // Create the Payment with an existing ID
        payment.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPaymentMockMvc.perform(post("/api/payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payment)))
            .andExpect(status().isBadRequest());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkPgaTrxIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentRepository.findAll().size();
        // set the field null
        payment.setPgaTrx(null);

        // Create the Payment, which fails.

        restPaymentMockMvc.perform(post("/api/payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payment)))
            .andExpect(status().isBadRequest());

        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkCreateDtIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentRepository.findAll().size();
        // set the field null
        payment.setCreateDt(null);

        // Create the Payment, which fails.

        restPaymentMockMvc.perform(post("/api/payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payment)))
            .andExpect(status().isBadRequest());

        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = paymentRepository.findAll().size();
        // set the field null
        payment.setStatus(null);

        // Create the Payment, which fails.

        restPaymentMockMvc.perform(post("/api/payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payment)))
            .andExpect(status().isBadRequest());

        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPayments() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get all the paymentList
        restPaymentMockMvc.perform(get("/api/payments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(payment.getId().intValue())))
            .andExpect(jsonPath("$.[*].pgaTrx").value(hasItem(DEFAULT_PGA_TRX)))
            .andExpect(jsonPath("$.[*].langCode").value(hasItem(DEFAULT_LANG_CODE)))
            .andExpect(jsonPath("$.[*].createDt").value(hasItem(DEFAULT_CREATE_DT.toString())))
            .andExpect(jsonPath("$.[*].dt").value(hasItem(DEFAULT_DT.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].extResultCode").value(hasItem(DEFAULT_EXT_RESULT_CODE)))
            .andExpect(jsonPath("$.[*].rrn").value(hasItem(DEFAULT_RRN)))
            .andExpect(jsonPath("$.[*].transactionDt").value(hasItem(DEFAULT_TRANSACTION_DT.toString())))
            .andExpect(jsonPath("$.[*].cardHolder").value(hasItem(DEFAULT_CARD_HOLDER)))
            .andExpect(jsonPath("$.[*].authCode").value(hasItem(DEFAULT_AUTH_CODE)))
            .andExpect(jsonPath("$.[*].pan").value(hasItem(DEFAULT_PAN)))
            .andExpect(jsonPath("$.[*].fullAuth").value(hasItem(DEFAULT_FULL_AUTH.booleanValue())))
            .andExpect(jsonPath("$.[*].paymentAmount").value(hasItem(DEFAULT_PAYMENT_AMOUNT.intValue())));
    }
    
    @Test
    @Transactional
    public void getPayment() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        // Get the payment
        restPaymentMockMvc.perform(get("/api/payments/{id}", payment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(payment.getId().intValue()))
            .andExpect(jsonPath("$.pgaTrx").value(DEFAULT_PGA_TRX))
            .andExpect(jsonPath("$.langCode").value(DEFAULT_LANG_CODE))
            .andExpect(jsonPath("$.createDt").value(DEFAULT_CREATE_DT.toString()))
            .andExpect(jsonPath("$.dt").value(DEFAULT_DT.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.extResultCode").value(DEFAULT_EXT_RESULT_CODE))
            .andExpect(jsonPath("$.rrn").value(DEFAULT_RRN))
            .andExpect(jsonPath("$.transactionDt").value(DEFAULT_TRANSACTION_DT.toString()))
            .andExpect(jsonPath("$.cardHolder").value(DEFAULT_CARD_HOLDER))
            .andExpect(jsonPath("$.authCode").value(DEFAULT_AUTH_CODE))
            .andExpect(jsonPath("$.pan").value(DEFAULT_PAN))
            .andExpect(jsonPath("$.fullAuth").value(DEFAULT_FULL_AUTH.booleanValue()))
            .andExpect(jsonPath("$.paymentAmount").value(DEFAULT_PAYMENT_AMOUNT.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPayment() throws Exception {
        // Get the payment
        restPaymentMockMvc.perform(get("/api/payments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePayment() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();

        // Update the payment
        Payment updatedPayment = paymentRepository.findById(payment.getId()).get();
        // Disconnect from session so that the updates on updatedPayment are not directly saved in db
        em.detach(updatedPayment);
        updatedPayment
            .pgaTrx(UPDATED_PGA_TRX)
            .langCode(UPDATED_LANG_CODE)
            .createDt(UPDATED_CREATE_DT)
            .dt(UPDATED_DT)
            .status(UPDATED_STATUS)
            .extResultCode(UPDATED_EXT_RESULT_CODE)
            .rrn(UPDATED_RRN)
            .transactionDt(UPDATED_TRANSACTION_DT)
            .cardHolder(UPDATED_CARD_HOLDER)
            .authCode(UPDATED_AUTH_CODE)
            .pan(UPDATED_PAN)
            .fullAuth(UPDATED_FULL_AUTH)
            .paymentAmount(UPDATED_PAYMENT_AMOUNT);

        restPaymentMockMvc.perform(put("/api/payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPayment)))
            .andExpect(status().isOk());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
        Payment testPayment = paymentList.get(paymentList.size() - 1);
        assertThat(testPayment.getPgaTrx()).isEqualTo(UPDATED_PGA_TRX);
        assertThat(testPayment.getLangCode()).isEqualTo(UPDATED_LANG_CODE);
        assertThat(testPayment.getCreateDt()).isEqualTo(UPDATED_CREATE_DT);
        assertThat(testPayment.getDt()).isEqualTo(UPDATED_DT);
        assertThat(testPayment.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testPayment.getExtResultCode()).isEqualTo(UPDATED_EXT_RESULT_CODE);
        assertThat(testPayment.getRrn()).isEqualTo(UPDATED_RRN);
        assertThat(testPayment.getTransactionDt()).isEqualTo(UPDATED_TRANSACTION_DT);
        assertThat(testPayment.getCardHolder()).isEqualTo(UPDATED_CARD_HOLDER);
        assertThat(testPayment.getAuthCode()).isEqualTo(UPDATED_AUTH_CODE);
        assertThat(testPayment.getPan()).isEqualTo(UPDATED_PAN);
        assertThat(testPayment.isFullAuth()).isEqualTo(UPDATED_FULL_AUTH);
        assertThat(testPayment.getPaymentAmount()).isEqualTo(UPDATED_PAYMENT_AMOUNT);
    }

    @Test
    @Transactional
    public void updateNonExistingPayment() throws Exception {
        int databaseSizeBeforeUpdate = paymentRepository.findAll().size();

        // Create the Payment

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPaymentMockMvc.perform(put("/api/payments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payment)))
            .andExpect(status().isBadRequest());

        // Validate the Payment in the database
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePayment() throws Exception {
        // Initialize the database
        paymentRepository.saveAndFlush(payment);

        int databaseSizeBeforeDelete = paymentRepository.findAll().size();

        // Delete the payment
        restPaymentMockMvc.perform(delete("/api/payments/{id}", payment.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Payment> paymentList = paymentRepository.findAll();
        assertThat(paymentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Payment.class);
        Payment payment1 = new Payment();
        payment1.setId(1L);
        Payment payment2 = new Payment();
        payment2.setId(payment1.getId());
        assertThat(payment1).isEqualTo(payment2);
        payment2.setId(2L);
        assertThat(payment1).isNotEqualTo(payment2);
        payment1.setId(null);
        assertThat(payment1).isNotEqualTo(payment2);
    }
}
