import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { take, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { PaymentService } from 'app/entities/payment/payment.service';
import { IPayment, Payment } from 'app/shared/model/payment.model';
import { PaymentStatus } from 'app/shared/model/enumerations/payment-status.model';

describe('Service Tests', () => {
  describe('Payment Service', () => {
    let injector: TestBed;
    let service: PaymentService;
    let httpMock: HttpTestingController;
    let elemDefault: IPayment;
    let expectedResult;
    let currentDate: moment.Moment;
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = {};
      injector = getTestBed();
      service = injector.get(PaymentService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new Payment(
        0,
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        currentDate,
        PaymentStatus.COMPLETED,
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        false,
        0
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            createDt: currentDate.format(DATE_TIME_FORMAT),
            dt: currentDate.format(DATE_TIME_FORMAT),
            transactionDt: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        service
          .find(123)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: elemDefault });
      });

      it('should create a Payment', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            createDt: currentDate.format(DATE_TIME_FORMAT),
            dt: currentDate.format(DATE_TIME_FORMAT),
            transactionDt: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            createDt: currentDate,
            dt: currentDate,
            transactionDt: currentDate
          },
          returnedFromService
        );
        service
          .create(new Payment(null))
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should update a Payment', () => {
        const returnedFromService = Object.assign(
          {
            pgaTrx: 'BBBBBB',
            langCode: 'BBBBBB',
            createDt: currentDate.format(DATE_TIME_FORMAT),
            dt: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
            extResultCode: 'BBBBBB',
            rrn: 'BBBBBB',
            transactionDt: currentDate.format(DATE_TIME_FORMAT),
            cardHolder: 'BBBBBB',
            authCode: 'BBBBBB',
            pan: 'BBBBBB',
            fullAuth: true,
            paymentAmount: 1
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createDt: currentDate,
            dt: currentDate,
            transactionDt: currentDate
          },
          returnedFromService
        );
        service
          .update(expected)
          .pipe(take(1))
          .subscribe(resp => (expectedResult = resp));
        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject({ body: expected });
      });

      it('should return a list of Payment', () => {
        const returnedFromService = Object.assign(
          {
            pgaTrx: 'BBBBBB',
            langCode: 'BBBBBB',
            createDt: currentDate.format(DATE_TIME_FORMAT),
            dt: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
            extResultCode: 'BBBBBB',
            rrn: 'BBBBBB',
            transactionDt: currentDate.format(DATE_TIME_FORMAT),
            cardHolder: 'BBBBBB',
            authCode: 'BBBBBB',
            pan: 'BBBBBB',
            fullAuth: true,
            paymentAmount: 1
          },
          elemDefault
        );
        const expected = Object.assign(
          {
            createDt: currentDate,
            dt: currentDate,
            transactionDt: currentDate
          },
          returnedFromService
        );
        service
          .query(expected)
          .pipe(
            take(1),
            map(resp => resp.body)
          )
          .subscribe(body => (expectedResult = body));
        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a Payment', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
