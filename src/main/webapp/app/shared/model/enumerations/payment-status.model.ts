export const enum PaymentStatus {
  COMPLETED = 'COMPLETED',
  PENDING = 'PENDING',
  CANCELLED = 'CANCELLED',
  PGA_ERROR = 'PGA_ERROR'
}
