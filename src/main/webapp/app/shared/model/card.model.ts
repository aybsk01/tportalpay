export interface ICard {
  id?: number;
  cardId?: string;
  expireDt?: string;
  requrrent?: boolean;
}

export class Card implements ICard {
  constructor(public id?: number, public cardId?: string, public expireDt?: string, public requrrent?: boolean) {
    this.requrrent = this.requrrent || false;
  }
}
