import { ICard } from 'app/shared/model/card.model';
import { IUser } from 'app/core/user/user.model';
import { IProductOrder } from 'app/shared/model/product-order.model';
import { Gender } from 'app/shared/model/enumerations/gender.model';

export interface ICustomer {
  id?: number;
  firstName?: string;
  lastName?: string;
  gender?: Gender;
  email?: string;
  phone?: string;
  addressLine1?: string;
  addressLine2?: string;
  city?: string;
  country?: string;
  card?: ICard;
  user?: IUser;
  orders?: IProductOrder[];
}

export class Customer implements ICustomer {
  constructor(
    public id?: number,
    public firstName?: string,
    public lastName?: string,
    public gender?: Gender,
    public email?: string,
    public phone?: string,
    public addressLine1?: string,
    public addressLine2?: string,
    public city?: string,
    public country?: string,
    public card?: ICard,
    public user?: IUser,
    public orders?: IProductOrder[]
  ) {}
}
