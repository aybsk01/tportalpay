import { Moment } from 'moment';
import { ICard } from 'app/shared/model/card.model';
import { IInvoice } from 'app/shared/model/invoice.model';
import { PaymentStatus } from 'app/shared/model/enumerations/payment-status.model';

export interface IPayment {
  id?: number;
  pgaTrx?: string;
  langCode?: string;
  createDt?: Moment;
  dt?: Moment;
  status?: PaymentStatus;
  extResultCode?: string;
  rrn?: string;
  transactionDt?: Moment;
  cardHolder?: string;
  authCode?: string;
  pan?: string;
  fullAuth?: boolean;
  paymentAmount?: number;
  card?: ICard;
  invoice?: IInvoice;
}

export class Payment implements IPayment {
  constructor(
    public id?: number,
    public pgaTrx?: string,
    public langCode?: string,
    public createDt?: Moment,
    public dt?: Moment,
    public status?: PaymentStatus,
    public extResultCode?: string,
    public rrn?: string,
    public transactionDt?: Moment,
    public cardHolder?: string,
    public authCode?: string,
    public pan?: string,
    public fullAuth?: boolean,
    public paymentAmount?: number,
    public card?: ICard,
    public invoice?: IInvoice
  ) {
    this.fullAuth = this.fullAuth || false;
  }
}
