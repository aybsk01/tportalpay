import { IInvoice } from 'app/shared/model/invoice.model';

export interface IMerchant {
  id?: number;
  pgaCode?: string;
  name?: string;
  invoices?: IInvoice[];
}

export class Merchant implements IMerchant {
  constructor(public id?: number, public pgaCode?: string, public name?: string, public invoices?: IInvoice[]) {}
}
