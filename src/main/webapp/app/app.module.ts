import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { TportalpaySharedModule } from 'app/shared/shared.module';
import { TportalpayCoreModule } from 'app/core/core.module';
import { TportalpayAppRoutingModule } from './app-routing.module';
import { TportalpayHomeModule } from './home/home.module';
import { TportalpayEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { JhiMainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';

@NgModule({
  imports: [
    BrowserModule,
    TportalpaySharedModule,
    TportalpayCoreModule,
    TportalpayHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    TportalpayEntityModule,
    TportalpayAppRoutingModule
  ],
  declarations: [JhiMainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [JhiMainComponent]
})
export class TportalpayAppModule {}
