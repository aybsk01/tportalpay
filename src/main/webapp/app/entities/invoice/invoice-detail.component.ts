import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IInvoice } from 'app/shared/model/invoice.model';

@Component({
  selector: 'jhi-invoice-detail',
  templateUrl: './invoice-detail.component.html'
})
export class InvoiceDetailComponent implements OnInit {
  invoice: IInvoice;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ invoice }) => {
      this.invoice = invoice;
    });
  }

  previousState() {
    window.history.back();
  }

  gotoPaymentPage() {
    window.location.href =
      'https://paymentpage-test-ecp.intervale.ru/apistend/?lang_code=RU&merch_id=1234567890&back_url_s=https://voyage.ru/success.jsp&back_url_f=http://voyage.ru/fail.jsp&o.order_id=' +
      this.invoice.productorder.id;
  }
}
