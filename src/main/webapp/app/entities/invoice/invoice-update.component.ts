import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IInvoice, Invoice } from 'app/shared/model/invoice.model';
import { InvoiceService } from './invoice.service';
import { IProductOrder } from 'app/shared/model/product-order.model';
import { ProductOrderService } from 'app/entities/product-order/product-order.service';
import { IMerchant } from 'app/shared/model/merchant.model';
import { MerchantService } from 'app/entities/merchant/merchant.service';

@Component({
  selector: 'jhi-invoice-update',
  templateUrl: './invoice-update.component.html'
})
export class InvoiceUpdateComponent implements OnInit {
  isSaving: boolean;

  productorders: IProductOrder[];

  merchants: IMerchant[];

  editForm = this.fb.group({
    id: [],
    code: [null, [Validators.required]],
    date: [null, [Validators.required]],
    details: [],
    status: [null, [Validators.required]],
    paymentMethod: [null, [Validators.required]],
    paymentDate: [],
    paymentAmount: [null, [Validators.required]],
    productorder: [null, Validators.required],
    merchant: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected invoiceService: InvoiceService,
    protected productOrderService: ProductOrderService,
    protected merchantService: MerchantService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ invoice }) => {
      this.updateForm(invoice);
    });
    this.productOrderService
      .query({ filter: 'invoice-is-null' })
      .pipe(
        filter((mayBeOk: HttpResponse<IProductOrder[]>) => mayBeOk.ok),
        map((response: HttpResponse<IProductOrder[]>) => response.body)
      )
      .subscribe(
        (res: IProductOrder[]) => {
          if (!this.editForm.get('productorder').value || !this.editForm.get('productorder').value.id) {
            this.productorders = res;
          } else {
            this.productOrderService
              .find(this.editForm.get('productorder').value.id)
              .pipe(
                filter((subResMayBeOk: HttpResponse<IProductOrder>) => subResMayBeOk.ok),
                map((subResponse: HttpResponse<IProductOrder>) => subResponse.body)
              )
              .subscribe(
                (subRes: IProductOrder) => (this.productorders = [subRes].concat(res)),
                (subRes: HttpErrorResponse) => this.onError(subRes.message)
              );
          }
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
    this.merchantService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMerchant[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMerchant[]>) => response.body)
      )
      .subscribe((res: IMerchant[]) => (this.merchants = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(invoice: IInvoice) {
    this.editForm.patchValue({
      id: invoice.id,
      code: invoice.code,
      date: invoice.date != null ? invoice.date.format(DATE_TIME_FORMAT) : null,
      details: invoice.details,
      status: invoice.status,
      paymentMethod: invoice.paymentMethod,
      paymentDate: invoice.paymentDate != null ? invoice.paymentDate.format(DATE_TIME_FORMAT) : null,
      paymentAmount: invoice.paymentAmount,
      productorder: invoice.productorder,
      merchant: invoice.merchant
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const invoice = this.createFromForm();
    if (invoice.id !== undefined) {
      this.subscribeToSaveResponse(this.invoiceService.update(invoice));
    } else {
      this.subscribeToSaveResponse(this.invoiceService.create(invoice));
    }
  }

  private createFromForm(): IInvoice {
    return {
      ...new Invoice(),
      id: this.editForm.get(['id']).value,
      code: this.editForm.get(['code']).value,
      date: this.editForm.get(['date']).value != null ? moment(this.editForm.get(['date']).value, DATE_TIME_FORMAT) : undefined,
      details: this.editForm.get(['details']).value,
      status: this.editForm.get(['status']).value,
      paymentMethod: this.editForm.get(['paymentMethod']).value,
      paymentDate:
        this.editForm.get(['paymentDate']).value != null ? moment(this.editForm.get(['paymentDate']).value, DATE_TIME_FORMAT) : undefined,
      paymentAmount: this.editForm.get(['paymentAmount']).value,
      productorder: this.editForm.get(['productorder']).value,
      merchant: this.editForm.get(['merchant']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IInvoice>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackProductOrderById(index: number, item: IProductOrder) {
    return item.id;
  }

  trackMerchantById(index: number, item: IMerchant) {
    return item.id;
  }
}
