import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'merchant',
        loadChildren: () => import('./merchant/merchant.module').then(m => m.TportalpayMerchantModule)
      },
      {
        path: 'product',
        loadChildren: () => import('./product/product.module').then(m => m.TportalpayProductModule)
      },
      {
        path: 'product-category',
        loadChildren: () => import('./product-category/product-category.module').then(m => m.TportalpayProductCategoryModule)
      },
      {
        path: 'customer',
        loadChildren: () => import('./customer/customer.module').then(m => m.TportalpayCustomerModule)
      },
      {
        path: 'product-order',
        loadChildren: () => import('./product-order/product-order.module').then(m => m.TportalpayProductOrderModule)
      },
      {
        path: 'order-item',
        loadChildren: () => import('./order-item/order-item.module').then(m => m.TportalpayOrderItemModule)
      },
      {
        path: 'payment',
        loadChildren: () => import('./payment/payment.module').then(m => m.TportalpayPaymentModule)
      },
      {
        path: 'card',
        loadChildren: () => import('./card/card.module').then(m => m.TportalpayCardModule)
      },
      {
        path: 'invoice',
        loadChildren: () => import('./invoice/invoice.module').then(m => m.TportalpayInvoiceModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class TportalpayEntityModule {}
