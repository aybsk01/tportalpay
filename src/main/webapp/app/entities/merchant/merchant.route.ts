import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Merchant } from 'app/shared/model/merchant.model';
import { MerchantService } from './merchant.service';
import { MerchantComponent } from './merchant.component';
import { MerchantDetailComponent } from './merchant-detail.component';
import { MerchantUpdateComponent } from './merchant-update.component';
import { MerchantDeletePopupComponent } from './merchant-delete-dialog.component';
import { IMerchant } from 'app/shared/model/merchant.model';

@Injectable({ providedIn: 'root' })
export class MerchantResolve implements Resolve<IMerchant> {
  constructor(private service: MerchantService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMerchant> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Merchant>) => response.ok),
        map((merchant: HttpResponse<Merchant>) => merchant.body)
      );
    }
    return of(new Merchant());
  }
}

export const merchantRoute: Routes = [
  {
    path: '',
    component: MerchantComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'tportalpayApp.merchant.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MerchantDetailComponent,
    resolve: {
      merchant: MerchantResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'tportalpayApp.merchant.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MerchantUpdateComponent,
    resolve: {
      merchant: MerchantResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'tportalpayApp.merchant.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MerchantUpdateComponent,
    resolve: {
      merchant: MerchantResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'tportalpayApp.merchant.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const merchantPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MerchantDeletePopupComponent,
    resolve: {
      merchant: MerchantResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'tportalpayApp.merchant.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
