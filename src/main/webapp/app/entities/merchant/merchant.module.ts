import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TportalpaySharedModule } from 'app/shared/shared.module';
import { MerchantComponent } from './merchant.component';
import { MerchantDetailComponent } from './merchant-detail.component';
import { MerchantUpdateComponent } from './merchant-update.component';
import { MerchantDeletePopupComponent, MerchantDeleteDialogComponent } from './merchant-delete-dialog.component';
import { merchantRoute, merchantPopupRoute } from './merchant.route';

const ENTITY_STATES = [...merchantRoute, ...merchantPopupRoute];

@NgModule({
  imports: [TportalpaySharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    MerchantComponent,
    MerchantDetailComponent,
    MerchantUpdateComponent,
    MerchantDeleteDialogComponent,
    MerchantDeletePopupComponent
  ],
  entryComponents: [MerchantDeleteDialogComponent]
})
export class TportalpayMerchantModule {}
