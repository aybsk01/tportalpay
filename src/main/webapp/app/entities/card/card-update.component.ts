import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ICard, Card } from 'app/shared/model/card.model';
import { CardService } from './card.service';

@Component({
  selector: 'jhi-card-update',
  templateUrl: './card-update.component.html'
})
export class CardUpdateComponent implements OnInit {
  isSaving: boolean;

  editForm = this.fb.group({
    id: [],
    cardId: [null, [Validators.maxLength(32)]],
    expireDt: [null, [Validators.maxLength(4)]],
    requrrent: []
  });

  constructor(protected cardService: CardService, protected activatedRoute: ActivatedRoute, private fb: FormBuilder) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ card }) => {
      this.updateForm(card);
    });
  }

  updateForm(card: ICard) {
    this.editForm.patchValue({
      id: card.id,
      cardId: card.cardId,
      expireDt: card.expireDt,
      requrrent: card.requrrent
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const card = this.createFromForm();
    if (card.id !== undefined) {
      this.subscribeToSaveResponse(this.cardService.update(card));
    } else {
      this.subscribeToSaveResponse(this.cardService.create(card));
    }
  }

  private createFromForm(): ICard {
    return {
      ...new Card(),
      id: this.editForm.get(['id']).value,
      cardId: this.editForm.get(['cardId']).value,
      expireDt: this.editForm.get(['expireDt']).value,
      requrrent: this.editForm.get(['requrrent']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICard>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
}
