import { Component, OnInit } from '@angular/core';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IPayment, Payment } from 'app/shared/model/payment.model';
import { PaymentService } from './payment.service';
import { ICard } from 'app/shared/model/card.model';
import { CardService } from 'app/entities/card/card.service';
import { IInvoice } from 'app/shared/model/invoice.model';
import { InvoiceService } from 'app/entities/invoice/invoice.service';

@Component({
  selector: 'jhi-payment-update',
  templateUrl: './payment-update.component.html'
})
export class PaymentUpdateComponent implements OnInit {
  isSaving: boolean;

  cards: ICard[];

  invoices: IInvoice[];

  editForm = this.fb.group({
    id: [],
    pgaTrx: [null, [Validators.required, Validators.maxLength(32)]],
    langCode: [null, [Validators.maxLength(2)]],
    createDt: [null, [Validators.required]],
    dt: [],
    status: [null, [Validators.required]],
    extResultCode: [],
    rrn: [],
    transactionDt: [],
    cardHolder: [null, [Validators.maxLength(26)]],
    authCode: [null, [Validators.maxLength(20)]],
    pan: [null, [Validators.maxLength(19)]],
    fullAuth: [],
    paymentAmount: [],
    card: [],
    invoice: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected paymentService: PaymentService,
    protected cardService: CardService,
    protected invoiceService: InvoiceService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ payment }) => {
      this.updateForm(payment);
    });
    this.cardService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<ICard[]>) => mayBeOk.ok),
        map((response: HttpResponse<ICard[]>) => response.body)
      )
      .subscribe((res: ICard[]) => (this.cards = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.invoiceService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IInvoice[]>) => mayBeOk.ok),
        map((response: HttpResponse<IInvoice[]>) => response.body)
      )
      .subscribe((res: IInvoice[]) => (this.invoices = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(payment: IPayment) {
    this.editForm.patchValue({
      id: payment.id,
      pgaTrx: payment.pgaTrx,
      langCode: payment.langCode,
      createDt: payment.createDt != null ? payment.createDt.format(DATE_TIME_FORMAT) : null,
      dt: payment.dt != null ? payment.dt.format(DATE_TIME_FORMAT) : null,
      status: payment.status,
      extResultCode: payment.extResultCode,
      rrn: payment.rrn,
      transactionDt: payment.transactionDt != null ? payment.transactionDt.format(DATE_TIME_FORMAT) : null,
      cardHolder: payment.cardHolder,
      authCode: payment.authCode,
      pan: payment.pan,
      fullAuth: payment.fullAuth,
      paymentAmount: payment.paymentAmount,
      card: payment.card,
      invoice: payment.invoice
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const payment = this.createFromForm();
    if (payment.id !== undefined) {
      this.subscribeToSaveResponse(this.paymentService.update(payment));
    } else {
      this.subscribeToSaveResponse(this.paymentService.create(payment));
    }
  }

  private createFromForm(): IPayment {
    return {
      ...new Payment(),
      id: this.editForm.get(['id']).value,
      pgaTrx: this.editForm.get(['pgaTrx']).value,
      langCode: this.editForm.get(['langCode']).value,
      createDt: this.editForm.get(['createDt']).value != null ? moment(this.editForm.get(['createDt']).value, DATE_TIME_FORMAT) : undefined,
      dt: this.editForm.get(['dt']).value != null ? moment(this.editForm.get(['dt']).value, DATE_TIME_FORMAT) : undefined,
      status: this.editForm.get(['status']).value,
      extResultCode: this.editForm.get(['extResultCode']).value,
      rrn: this.editForm.get(['rrn']).value,
      transactionDt:
        this.editForm.get(['transactionDt']).value != null
          ? moment(this.editForm.get(['transactionDt']).value, DATE_TIME_FORMAT)
          : undefined,
      cardHolder: this.editForm.get(['cardHolder']).value,
      authCode: this.editForm.get(['authCode']).value,
      pan: this.editForm.get(['pan']).value,
      fullAuth: this.editForm.get(['fullAuth']).value,
      paymentAmount: this.editForm.get(['paymentAmount']).value,
      card: this.editForm.get(['card']).value,
      invoice: this.editForm.get(['invoice']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPayment>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackCardById(index: number, item: ICard) {
    return item.id;
  }

  trackInvoiceById(index: number, item: IInvoice) {
    return item.id;
  }
}
