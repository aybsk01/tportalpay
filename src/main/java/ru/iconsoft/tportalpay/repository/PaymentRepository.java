package ru.iconsoft.tportalpay.repository;
import ru.iconsoft.tportalpay.domain.Payment;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Payment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {
    @Query("select p from Payment p where p.invoice.id = ?1 and ((p.status = ru.iconsoft.tportalpay.domain.enumeration.PaymentStatus.PENDING) or (p.status = ru.iconsoft.tportalpay.domain.enumeration.PaymentStatus.COMPLETED))")
    List<Payment> findByInvoiceIdPendingOrCompleted(Long invoiceId);
    @Query("select p from Payment p where (p.invoice.id = ?1) and (p.status = ru.iconsoft.tportalpay.domain.enumeration.PaymentStatus.PENDING)")
    List<Payment> findByInvoiceIdPending(Long invoiceId);
    @Query("select p from Payment p where (p.card.id = ?1)")
    List<Payment> findByCardId(Long cardId);
}
