package ru.iconsoft.tportalpay.repository;
import ru.iconsoft.tportalpay.domain.Merchant;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Merchant entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MerchantRepository extends JpaRepository<Merchant, Long> {
    @Query("select m from Merchant m where m.pgaCode = ?1")
    Merchant findByPgaCode(String pgaCode);
}
