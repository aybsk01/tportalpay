package ru.iconsoft.tportalpay.repository;
import ru.iconsoft.tportalpay.domain.Invoice;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Invoice entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, Long> {
    @Query("select i from Invoice i where i.productorder.id = ?1")
    Invoice findByOrderId(Long orderId);
}
