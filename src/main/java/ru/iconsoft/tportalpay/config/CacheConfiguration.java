package ru.iconsoft.tportalpay.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import org.hibernate.cache.jcache.ConfigSettings;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache = jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, ru.iconsoft.tportalpay.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, ru.iconsoft.tportalpay.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, ru.iconsoft.tportalpay.domain.User.class.getName());
            createCache(cm, ru.iconsoft.tportalpay.domain.Authority.class.getName());
            createCache(cm, ru.iconsoft.tportalpay.domain.User.class.getName() + ".authorities");
            createCache(cm, ru.iconsoft.tportalpay.domain.Merchant.class.getName());
            createCache(cm, ru.iconsoft.tportalpay.domain.Merchant.class.getName() + ".invoices");
            createCache(cm, ru.iconsoft.tportalpay.domain.Product.class.getName());
            createCache(cm, ru.iconsoft.tportalpay.domain.ProductCategory.class.getName());
            createCache(cm, ru.iconsoft.tportalpay.domain.ProductCategory.class.getName() + ".products");
            createCache(cm, ru.iconsoft.tportalpay.domain.Customer.class.getName());
            createCache(cm, ru.iconsoft.tportalpay.domain.Customer.class.getName() + ".orders");
            createCache(cm, ru.iconsoft.tportalpay.domain.ProductOrder.class.getName());
            createCache(cm, ru.iconsoft.tportalpay.domain.ProductOrder.class.getName() + ".orderItems");
            createCache(cm, ru.iconsoft.tportalpay.domain.OrderItem.class.getName());
            createCache(cm, ru.iconsoft.tportalpay.domain.Payment.class.getName());
            createCache(cm, ru.iconsoft.tportalpay.domain.Card.class.getName());
            createCache(cm, ru.iconsoft.tportalpay.domain.Invoice.class.getName());
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache != null) {
            cm.destroyCache(cacheName);
        }
        cm.createCache(cacheName, jcacheConfiguration);
    }

}
