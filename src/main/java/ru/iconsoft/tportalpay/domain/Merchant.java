package ru.iconsoft.tportalpay.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Merchant.
 */
@Entity
@Table(name = "merchant")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Merchant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 32)
    @Column(name = "pga_code", length = 32, nullable = false)
    private String pgaCode;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "merchant")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Invoice> invoices = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPgaCode() {
        return pgaCode;
    }

    public Merchant pgaCode(String pgaCode) {
        this.pgaCode = pgaCode;
        return this;
    }

    public void setPgaCode(String pgaCode) {
        this.pgaCode = pgaCode;
    }

    public String getName() {
        return name;
    }

    public Merchant name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Invoice> getInvoices() {
        return invoices;
    }

    public Merchant invoices(Set<Invoice> invoices) {
        this.invoices = invoices;
        return this;
    }

    public Merchant addInvoice(Invoice invoice) {
        this.invoices.add(invoice);
        invoice.setMerchant(this);
        return this;
    }

    public Merchant removeInvoice(Invoice invoice) {
        this.invoices.remove(invoice);
        invoice.setMerchant(null);
        return this;
    }

    public void setInvoices(Set<Invoice> invoices) {
        this.invoices = invoices;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Merchant)) {
            return false;
        }
        return id != null && id.equals(((Merchant) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Merchant{" +
            "id=" + getId() +
            ", pgaCode='" + getPgaCode() + "'" +
            ", name='" + getName() + "'" +
            "}";
    }
}
