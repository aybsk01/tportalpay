package ru.iconsoft.tportalpay.domain;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

/**
 * A Card.
 */
@Entity
@Table(name = "card")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Card implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 32)
    @Column(name = "card_id", length = 32)
    private String cardId;

    @Size(max = 4)
    @Column(name = "expire_dt", length = 4)
    private String expireDt;

    @Column(name = "requrrent")
    private Boolean requrrent;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCardId() {
        return cardId;
    }

    public Card cardId(String cardId) {
        this.cardId = cardId;
        return this;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getExpireDt() {
        return expireDt;
    }

    public Card expireDt(String expireDt) {
        this.expireDt = expireDt;
        return this;
    }

    public void setExpireDt(String expireDt) {
        this.expireDt = expireDt;
    }

    public Boolean isRequrrent() {
        return requrrent;
    }

    public Card requrrent(Boolean requrrent) {
        this.requrrent = requrrent;
        return this;
    }

    public void setRequrrent(Boolean requrrent) {
        this.requrrent = requrrent;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Card)) {
            return false;
        }
        return id != null && id.equals(((Card) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Card{" +
            "id=" + getId() +
            ", cardId='" + getCardId() + "'" +
            ", expireDt='" + getExpireDt() + "'" +
            ", requrrent='" + isRequrrent() + "'" +
            "}";
    }
}
