package ru.iconsoft.tportalpay.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

import ru.iconsoft.tportalpay.domain.enumeration.PaymentStatus;

/**
 * A Payment.
 */
@Entity
@Table(name = "payment")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Payment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 32)
    @Column(name = "pga_trx", length = 32, nullable = false, unique = true)
    private String pgaTrx;

    @Size(max = 2)
    @Column(name = "lang_code", length = 2)
    private String langCode;

    @NotNull
    @Column(name = "create_dt", nullable = false)
    private Instant createDt;

    @Column(name = "dt")
    private Instant dt;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private PaymentStatus status;

    @Column(name = "ext_result_code")
    private String extResultCode;

    @Column(name = "rrn")
    private String rrn;

    @Column(name = "transaction_dt")
    private Instant transactionDt;

    @Size(max = 26)
    @Column(name = "card_holder", length = 26)
    private String cardHolder;

    @Size(max = 20)
    @Column(name = "auth_code", length = 20)
    private String authCode;

    @Size(max = 19)
    @Column(name = "pan", length = 19)
    private String pan;

    @Column(name = "full_auth")
    private Boolean fullAuth;

    @Column(name = "payment_amount", precision = 21, scale = 2)
    private BigDecimal paymentAmount;

    @ManyToOne
    @JsonIgnoreProperties("payments")
    private Card card;

    @ManyToOne(optional = false)
    @NotNull
    @JsonIgnoreProperties("payments")
    private Invoice invoice;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPgaTrx() {
        return pgaTrx;
    }

    public Payment pgaTrx(String pgaTrx) {
        this.pgaTrx = pgaTrx;
        return this;
    }

    public void setPgaTrx(String pgaTrx) {
        this.pgaTrx = pgaTrx;
    }

    public String getLangCode() {
        return langCode;
    }

    public Payment langCode(String langCode) {
        this.langCode = langCode;
        return this;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public Instant getCreateDt() {
        return createDt;
    }

    public Payment createDt(Instant createDt) {
        this.createDt = createDt;
        return this;
    }

    public void setCreateDt(Instant createDt) {
        this.createDt = createDt;
    }

    public Instant getDt() {
        return dt;
    }

    public Payment dt(Instant dt) {
        this.dt = dt;
        return this;
    }

    public void setDt(Instant dt) {
        this.dt = dt;
    }

    public PaymentStatus getStatus() {
        return status;
    }

    public Payment status(PaymentStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(PaymentStatus status) {
        this.status = status;
    }

    public String getExtResultCode() {
        return extResultCode;
    }

    public Payment extResultCode(String extResultCode) {
        this.extResultCode = extResultCode;
        return this;
    }

    public void setExtResultCode(String extResultCode) {
        this.extResultCode = extResultCode;
    }

    public String getRrn() {
        return rrn;
    }

    public Payment rrn(String rrn) {
        this.rrn = rrn;
        return this;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public Instant getTransactionDt() {
        return transactionDt;
    }

    public Payment transactionDt(Instant transactionDt) {
        this.transactionDt = transactionDt;
        return this;
    }

    public void setTransactionDt(Instant transactionDt) {
        this.transactionDt = transactionDt;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public Payment cardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
        return this;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public String getAuthCode() {
        return authCode;
    }

    public Payment authCode(String authCode) {
        this.authCode = authCode;
        return this;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getPan() {
        return pan;
    }

    public Payment pan(String pan) {
        this.pan = pan;
        return this;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public Boolean isFullAuth() {
        return fullAuth;
    }

    public Payment fullAuth(Boolean fullAuth) {
        this.fullAuth = fullAuth;
        return this;
    }

    public void setFullAuth(Boolean fullAuth) {
        this.fullAuth = fullAuth;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public Payment paymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
        return this;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public Card getCard() {
        return card;
    }

    public Payment card(Card card) {
        this.card = card;
        return this;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public Payment invoice(Invoice invoice) {
        this.invoice = invoice;
        return this;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Payment)) {
            return false;
        }
        return id != null && id.equals(((Payment) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Payment{" +
            "id=" + getId() +
            ", pgaTrx='" + getPgaTrx() + "'" +
            ", langCode='" + getLangCode() + "'" +
            ", createDt='" + getCreateDt() + "'" +
            ", dt='" + getDt() + "'" +
            ", status='" + getStatus() + "'" +
            ", extResultCode='" + getExtResultCode() + "'" +
            ", rrn='" + getRrn() + "'" +
            ", transactionDt='" + getTransactionDt() + "'" +
            ", cardHolder='" + getCardHolder() + "'" +
            ", authCode='" + getAuthCode() + "'" +
            ", pan='" + getPan() + "'" +
            ", fullAuth='" + isFullAuth() + "'" +
            ", paymentAmount=" + getPaymentAmount() +
            "}";
    }
}
