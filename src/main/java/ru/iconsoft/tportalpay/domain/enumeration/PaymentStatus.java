package ru.iconsoft.tportalpay.domain.enumeration;

/**
 * The PaymentStatus enumeration.
 */
public enum PaymentStatus {
    COMPLETED, PENDING, CANCELLED, PGA_ERROR
}
