package ru.iconsoft.tportalpay.domain.enumeration;

/**
 * The InvoiceStatus enumeration.
 */
public enum InvoiceStatus {
    PAID, ISSUED, CANCELLED
}
