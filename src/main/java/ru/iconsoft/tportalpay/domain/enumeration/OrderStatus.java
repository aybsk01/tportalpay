package ru.iconsoft.tportalpay.domain.enumeration;

/**
 * The OrderStatus enumeration.
 */
public enum OrderStatus {
    COMPLETED, PENDING, CANCELLED
}
