package ru.iconsoft.tportalpay.service.pga.dto;

public class RegisterPaymentCardParams {
    private String id;
    private String registered;
    private String expiry;
    private String recurrent;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getRecurrent() {
        return recurrent;
    }

    public void setRecurrent(String recurrent) {
        this.recurrent = recurrent;
    }

    public Boolean isValid(){
        return (registered != null) && (id != null) && (expiry != null);
    }

    public String getRegistered() {
        return registered;
    }

    public void setRegistered(String registered) {
        this.registered = registered;
    }
}
