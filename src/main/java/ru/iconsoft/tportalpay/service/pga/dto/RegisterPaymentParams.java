package ru.iconsoft.tportalpay.service.pga.dto;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.Instant;
import java.util.Date;

public class RegisterPaymentParams {

    public RegisterPaymentParams(){}

    private String rrn;
    @DateTimeFormat(pattern = "MMddHHmmss")
    private Date transmissionDateTime;
    private String cardHolder;
    private String authcode;
    private String maskedPan;
    private String isFullyAuthenticated;

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public Date getTransmissionDateTime() {
        return transmissionDateTime;
    }

    public void setTransmissionDateTime(Date transmissionDateTime) {
        this.transmissionDateTime = transmissionDateTime;
    }

    public String getCardHolder() {
        return cardHolder;
    }

    public void setCardHolder(String cardHolder) {
        this.cardHolder = cardHolder;
    }

    public String getAuthcode() {
        return authcode;
    }

    public void setAuthcode(String authcode) {
        this.authcode = authcode;
    }

    public String getMaskedPan() {
        return maskedPan;
    }

    public void setMaskedPan(String maskedPan) {
        this.maskedPan = maskedPan;
    }

    public String getIsFullyAuthenticated() {
        return isFullyAuthenticated;
    }

    public void setIsFullyAuthenticated(String isFullyAuthenticated) {
        this.isFullyAuthenticated = isFullyAuthenticated;
    }
}
