package ru.iconsoft.tportalpay.service.pga;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iconsoft.tportalpay.domain.*;
import ru.iconsoft.tportalpay.domain.enumeration.InvoiceStatus;
import ru.iconsoft.tportalpay.domain.enumeration.OrderStatus;
import ru.iconsoft.tportalpay.domain.enumeration.PaymentStatus;
import ru.iconsoft.tportalpay.repository.*;
import ru.iconsoft.tportalpay.service.pga.dto.*;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class PgaService {

    private MerchantRepository merchantRepository;
    private ProductOrderRepository productOrderRepository;
    private PaymentRepository paymentRepository;
    private InvoiceRepository invoiceRepository;
    private CardRepository cardRepository;
    private CustomerRepository customerRepository;

    public PgaService(MerchantRepository merchantRepository, ProductOrderRepository productOrderRepository,
                      PaymentRepository paymentRepository, InvoiceRepository invoiceRepository,
                      CardRepository cardRepository, CustomerRepository customerRepository){
        this.merchantRepository = merchantRepository;
        this.productOrderRepository = productOrderRepository;
        this.paymentRepository = paymentRepository;
        this.invoiceRepository = invoiceRepository;
        this.cardRepository = cardRepository;
        this.customerRepository = customerRepository;
    }

    public CpaResponseDto startPaymentTransaction(CpaRequestDto payParams){
        if ((payParams == null) || !payParams.isValid())
            return new CpaResponseDto(2, "Error incoming request");
        Merchant merchant = merchantRepository.findByPgaCode(payParams.getMerch_id());
        if (merchant == null)
            return new CpaResponseDto(2, "Error. Incorrect merchant information");
        Optional<ProductOrder> order = productOrderRepository.findById(payParams.getO().getOrder_id());
        if (!order.isPresent())
            return new CpaResponseDto(2, "Error. Order not found");
        Invoice invoice = invoiceRepository.findByOrderId(payParams.getO().getOrder_id());
        if (invoice == null)
            return new CpaResponseDto(2, "Error. Invoice not found");
        List<Payment> invocesPayment = paymentRepository.findByInvoiceIdPendingOrCompleted(invoice.getId());
        if (invocesPayment.size() > 0)
            return new CpaResponseDto(2, "Error. Payment cannot be started for this order at this time.");

        Payment payment = new Payment();
        payment.setPgaTrx(payParams.getTrx_id());
        payment.setStatus(PaymentStatus.PENDING);
        payment.setInvoice(invoice);
        payment.setCreateDt(payParams.getTs().toInstant());
        payment.setLangCode(payParams.getLang_code());
        paymentRepository.save(payment);
        Long payId = payment.getId();

        CpaResponceCard customerCard = null;
        Customer customer = order.get().getCustomer();
        if ((customer != null) && (customer.getCard() != null) && (customer.getCard().getId() != null)){
            List<Payment> initCardPays = paymentRepository.findByCardId(customer.getCard().getId());
            if ((initCardPays != null) && (initCardPays.size() > 0)){
                //
                customerCard = new CpaResponceCard();
                customerCard.setTrxId(initCardPays.get(0).getPgaTrx());
                customerCard.setId(customer.getCard().getCardId());
            }
        }

        //Тут нужно выяснить что нужно будет показывать на платежной странице
        CpaResponseDto responseDto = new CpaResponseDto(payId.toString(),
            new PgaResponseResult(1, "Payment available"),
            new CpaResponsePurchase(invoice.getDetails(), order.get().getCustomer().getFirstName(),
                                    new CpaResponsePurchaseAccount(invoice.getId().toString(), invoice.getPaymentAmount()))
        );
        if (customerCard != null){
            responseDto.setCard(customerCard);
        }

        return responseDto;
    }

    public RegisterPaymentResponse completePaymentTransaction(RegisterPaymentRequest rpReq, String fullUrl){
        if (!PgaSignUtil.checkUrlSign(fullUrl)){
            return new RegisterPaymentResponse(2, "Signature failed");
        }
        if (rpReq == null)
            return new RegisterPaymentResponse(2, "Error incoming request");
        Merchant merchant = merchantRepository.findByPgaCode(rpReq.getMerch_id());
        if (merchant == null)
            return new RegisterPaymentResponse(2, "Error. Incorrect merchant information");
        Optional<ProductOrder> order = productOrderRepository.findById(rpReq.getO().getOrder_id());
        if (!order.isPresent())
            return new RegisterPaymentResponse(2, "Error. Order not found");
        Invoice invoice = invoiceRepository.findByOrderId(rpReq.getO().getOrder_id());
        if (invoice == null)
            return new RegisterPaymentResponse(2, "Error. Invoice not found");
        List<Payment> invocesPayment = paymentRepository.findByInvoiceIdPending(invoice.getId());
        if (invocesPayment.size() != 1){
            return new RegisterPaymentResponse(2, "Error. Not found pending payment for complete transaction");
        }

        Payment payment = invocesPayment.get(0);
        payment.setStatus(rpReq.getResult_code().equals(1) ? PaymentStatus.COMPLETED : PaymentStatus.PGA_ERROR);
        payment.setExtResultCode(rpReq.getExt_result_code());
        payment.setPaymentAmount(rpReq.getAmount());
        payment.setTransactionDt(rpReq.getTs().toInstant());

        RegisterPaymentParams paymentParams = rpReq.getP();
        payment.setDt(paymentParams.getTransmissionDateTime().toInstant());
        payment.setRrn(paymentParams.getRrn());
        payment.setCardHolder(paymentParams.getCardHolder());
        payment.setAuthCode(paymentParams.getAuthcode());
        payment.setPan(paymentParams.getMaskedPan());
        payment.setFullAuth(paymentParams.getIsFullyAuthenticated().equals("Y"));

        RegisterPaymentCardParams cardParams = rpReq.getCard();
        if ((cardParams != null) && (cardParams.isValid()) && (cardParams.getRegistered().equals("Y"))){
            Card card = new Card();
            card.setCardId(cardParams.getId());
            card.setExpireDt(cardParams.getExpiry());
            if ((cardParams.getRecurrent() != null) && (cardParams.getRecurrent().equals("Y"))){
                card.setRequrrent(true);
            }
            payment.setCard(card);
            cardRepository.save(card);
            Customer customer = customerRepository.findById(order.get().getCustomer().getId()).get();
            customer.setCard(card);
            customerRepository.save(customer);
            invoice.setStatus(InvoiceStatus.PAID);
            invoiceRepository.save(invoice);
            order.get().setStatus(OrderStatus.COMPLETED);
            productOrderRepository.save(order.get());
        }
        paymentRepository.save(payment);
        return new RegisterPaymentResponse(1, "OK");
    }
}
