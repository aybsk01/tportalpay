package ru.iconsoft.tportalpay.service.pga.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import java.math.BigDecimal;

@XmlAccessorType(XmlAccessType.FIELD)
public class CpaResponsePurchaseAccount {

    public CpaResponsePurchaseAccount(){}

    public CpaResponsePurchaseAccount(String id, BigDecimal amount){
        this.id = id;
        this.amount = amount;
    }

    private String id;
    private BigDecimal amount;
    private Long fee = 0L;
    private Integer currency = 643;
    private Integer exponent = 2;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getFee() {
        return fee;
    }

    public void setFee(Long fee) {
        this.fee = fee;
    }

    public Integer getCurrency() {
        return currency;
    }

    public void setCurrency(Integer currency) {
        this.currency = currency;
    }

    public Integer getExponent() {
        return exponent;
    }

    public void setExponent(Integer exponent) {
        this.exponent = exponent;
    }
}
