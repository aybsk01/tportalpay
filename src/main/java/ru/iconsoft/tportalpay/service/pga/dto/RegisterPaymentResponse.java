package ru.iconsoft.tportalpay.service.pga.dto;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="register-payment-response")
public class RegisterPaymentResponse {

    public RegisterPaymentResponse(){}

    public RegisterPaymentResponse(Integer code, String desc){
        result = new PgaResponseResult(code, desc);
    }

    private PgaResponseResult result;

    public PgaResponseResult getResult() {
        return result;
    }

    public void setResult(PgaResponseResult result) {
        this.result = result;
    }
}
