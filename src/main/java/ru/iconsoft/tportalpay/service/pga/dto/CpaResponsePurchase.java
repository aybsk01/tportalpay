package ru.iconsoft.tportalpay.service.pga.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class CpaResponsePurchase {
    public CpaResponsePurchase(){}

    public CpaResponsePurchase(String shortDesc, String longDesc, CpaResponsePurchaseAccount account){
        this.shortDesc = shortDesc;
        this.longDesc = longDesc;
        this.account = account;
    }

    private String shortDesc;
    private String longDesc;
    @XmlElement(name = "account-amount")
    private CpaResponsePurchaseAccount account;

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getLongDesc() {
        return longDesc;
    }

    public void setLongDesc(String longDesc) {
        this.longDesc = longDesc;
    }

    public CpaResponsePurchaseAccount getAccount() {
        return account;
    }

    public void setAccount(CpaResponsePurchaseAccount account) {
        this.account = account;
    }
}
