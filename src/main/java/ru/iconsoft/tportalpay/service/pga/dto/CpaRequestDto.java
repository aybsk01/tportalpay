package ru.iconsoft.tportalpay.service.pga.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class CpaRequestDto {
    @NotNull
    private String merch_id;
    @NotNull
    private String trx_id;
    private String lang_code;
    @DateTimeFormat(pattern = "yyyyMMdd HH:mm:ss")
    @NotNull
    private Date ts;
    @NotNull
    private PgaPaymentOrderData o;

    public String getMerch_id() {
        return merch_id;
    }

    public void setMerch_id(String merch_id) {
        this.merch_id = merch_id;
    }


    public String getTrx_id() {
        return trx_id;
    }

    public void setTrx_id(String trx_id) {
        this.trx_id = trx_id;
    }

    public String getLang_code() {
        return lang_code;
    }

    public void setLang_code(String lang_code) {
        this.lang_code = lang_code;
    }

    public Date getTs() {
        return ts;
    }

    public void setTs(Date ts) {
        this.ts = ts;
    }

    public Boolean isValid(){
        return (merch_id != null) && (trx_id != null) && (o != null);
    }

    public PgaPaymentOrderData getO() {
        return o;
    }

    public void setO(PgaPaymentOrderData o) {
        this.o = o;
    }
}
