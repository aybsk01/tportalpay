package ru.iconsoft.tportalpay.service.pga.dto;

import org.springframework.format.annotation.DateTimeFormat;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

public class RegisterPaymentRequest {

    public RegisterPaymentRequest(){
    }

    private String merch_id;
    @NotNull
    private String trx_id;
    @NotNull
    private String merchant_trx;
    @NotNull
    private Integer result_code;
    private String ext_result_code;
    @NotNull
    private BigDecimal amount;
    private String account_id;
    private RegisterPaymentParams p;
    private RegisterPaymentCardParams card;
    private PgaPaymentOrderData o;

    @DateTimeFormat(pattern = "yyyyMMdd HH:mm:ss")
    @NotNull
    private Date ts;
    @NotNull
    private String signature;

    public String getMerch_id() {
        return merch_id;
    }

    public void setMerch_id(String merch_id) {
        this.merch_id = merch_id;
    }

    public String getTrx_id() {
        return trx_id;
    }

    public void setTrx_id(String trx_id) {
        this.trx_id = trx_id;
    }

    public String getMerchant_trx() {
        return merchant_trx;
    }

    public void setMerchant_trx(String merchant_trx) {
        this.merchant_trx = merchant_trx;
    }

    public Integer getResult_code() {
        return result_code;
    }

    public void setResult_code(Integer result_code) {
        this.result_code = result_code;
    }

    public String getExt_result_code() {
        return ext_result_code;
    }

    public void setExt_result_code(String ext_result_code) {
        this.ext_result_code = ext_result_code;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public Date getTs() {
        return ts;
    }

    public void setTs(Date ts) {
        this.ts = ts;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    public RegisterPaymentParams getP() {
        return p;
    }

    public void setP(RegisterPaymentParams p) {
        this.p = p;
    }

    public RegisterPaymentCardParams getCard() {
        return card;
    }

    public void setCard(RegisterPaymentCardParams card) {
        this.card = card;
    }

    public PgaPaymentOrderData getO() {
        return o;
    }

    public void setO(PgaPaymentOrderData o) {
        this.o = o;
    }
}
