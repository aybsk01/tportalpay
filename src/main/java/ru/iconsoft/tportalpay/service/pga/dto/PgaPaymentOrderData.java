package ru.iconsoft.tportalpay.service.pga.dto;

public class PgaPaymentOrderData {
    private Long order_id;

    public Long getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Long order_id) {
        this.order_id = order_id;
    }
}
