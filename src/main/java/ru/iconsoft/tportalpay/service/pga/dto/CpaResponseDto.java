package ru.iconsoft.tportalpay.service.pga.dto;

import javax.xml.bind.annotation.*;
import java.io.Serializable;

@XmlRootElement(name = "payment-avail-response")
@XmlAccessorType(XmlAccessType.FIELD)
public class CpaResponseDto implements Serializable {

    public CpaResponseDto(){}

    public CpaResponseDto(Integer code, String desc){
        this.result = new PgaResponseResult(code, desc);
    }

    public CpaResponseDto(String merchantTrx, PgaResponseResult result, CpaResponsePurchase purchase){
        this.merchantTrx = merchantTrx;
        this.result = result;
        this.purchase = purchase;
    }

    private String merchantTrx;

    private String submerchant;

    public String getMerchantTrx() {
        return merchantTrx;
    }

    public void setMerchantTrx(String merchantTrx) {
        this.merchantTrx = merchantTrx;
    }

    @XmlElement(required = true)
    private PgaResponseResult result;

    private CpaResponsePurchase purchase;

    private CpaResponceCard card;

    //orderparams - нет в примере, уточнить что это

    @XmlElement(name="transaction-type")
    private String transactionType;

    public PgaResponseResult getResult() {
        return result;
    }

    public void setResult(PgaResponseResult result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "CpaResponseDto{" +
            "merchantTrx='" + merchantTrx + '\''+
            '}';
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public CpaResponsePurchase getPurchase() {
        return purchase;
    }

    public void setPurchase(CpaResponsePurchase purchase) {
        this.purchase = purchase;
    }

    public String getSubmerchant() {
        return submerchant;
    }

    public void setSubmerchant(String submerchant) {
        this.submerchant = submerchant;
    }

    public CpaResponceCard getCard() {
        return card;
    }

    public void setCard(CpaResponceCard card) {
        this.card = card;
    }
}
