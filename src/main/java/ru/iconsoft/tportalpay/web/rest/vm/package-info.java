/**
 * View Models used by Spring MVC REST controllers.
 */
package ru.iconsoft.tportalpay.web.rest.vm;
