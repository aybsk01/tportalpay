package ru.iconsoft.tportalpay.web.rest;


import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.iconsoft.tportalpay.domain.Card;
import ru.iconsoft.tportalpay.domain.Payment;
import ru.iconsoft.tportalpay.repository.PaymentRepository;
import ru.iconsoft.tportalpay.service.pga.PgaService;
import ru.iconsoft.tportalpay.service.pga.PgaSignUtil;
import ru.iconsoft.tportalpay.service.pga.dto.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/pga")
public class PgaResource {
    private final Logger log = LoggerFactory.getLogger(PgaResource.class);

    private final PgaService pgaService;

    public PgaResource(PgaService pgaService) {
        this.pgaService = pgaService;
    }

    @GetMapping(value="/cpareq", produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<CpaResponseDto> checkPaymentAvailable(@Valid CpaRequestDto cpaReq){
        log.trace("CheckPaymentAvailable: ", cpaReq);
        if (cpaReq == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        CpaResponseDto cpaRes = pgaService.startPaymentTransaction(cpaReq);
        Optional<CpaResponseDto> result = Optional.of(cpaRes);
        return ResponseUtil.wrapOrNotFound(result);
    }

    @GetMapping(value="/rpreq", produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<RegisterPaymentResponse> registerPaymentHandler(
        @Valid RegisterPaymentRequest rpReq, HttpServletRequest req){
        log.trace("RegisterPaymentHandler: ", rpReq);
        //url for check signature
        String fullUrl = new ServletServerHttpRequest(req).getURI().toString();
        if (rpReq == null) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        RegisterPaymentResponse rpRes = pgaService.completePaymentTransaction(rpReq, fullUrl);
        Optional<RegisterPaymentResponse> result = Optional.of(rpRes);
        return ResponseUtil.wrapOrNotFound(result);
    }
}
